# Camp Site Reservation System  

### A RESTful API that manages campsite reservation. 

## Run the program

### Executable JAR
```bash
$ git clone git@bitbucket.org:gbarsky/campbooking.git
$ cd campbooking
$ mvn package -DskipTests
$ java -jar target/campbooking-<version>.jar
```
To run the service with mysql database:
```bash
$ git clone git@bitbucket.org:gbarsky/campbooking.git
$ cd campbooking
$ mvn package -DskipTests
$ java -jar -Dspring.profiles.active=mysql target/campbooking-<version>.jar
```

The Swagger UI is available at `http://localhost:8989/swagger-ui.html`.

## Tests
### Maven
* Run only unit tests:
```bash
$ mvn clean test
```

In order to test via Swagger UI add a new reservation for example, expand the `POST` operation. Then click on the `Try it out`, add the payload below to the `Request Body` text area, and click on the `Execute`:
```json
{
  "uuid": "94bf6026-b668-11eb-8529-0242ac130003",
  "email": "developer@gmail.com",
  "fullName": "Dev Dev",
  "startDate": "2021-05-23",
  "endDate": "2021-05-24",
  "active": true
}
```

### H2 Console
H2 console is available at `http://localhost:8989/h2-console`.

Fill the login form as follows and click on Connect:
* Saved Settings: **Generic H2 (Embedded)**
* Setting Name: **Generic H2 (Embedded)**
* Driver class: **org.h2.Driver**
* JDBC URL: **jdbc:h2:mem:campsite;MODE=MySQL**
* User Name: **sa**
* Password:

### PostMan Collection to run the operations
Import to a PostMan Camp Site Reservation.postman_collection.json to be able to 
execute operations via PostMan

### Concurrent Bookings Creation
**Note**: There is some issue currently with that, which I believe has something to do 
with pessimistic lock setting in H2
If we run with "mysql" profile with mysql database - this concurrency test works fine 

Make sure service is up and running
```bash
$ cd campbooking
$ ./concurrency-test.sh http://localhost:8989
```

The response should be as follows after formatting, i.e., only one booking was created:
```json
{
   "id":1,
   "version":0,
   "uuid":"94bf6026-b668-11eb-8529-0242ac130003",
   "email":"dev1@gmail.com",
   "fullName":"Developer_1",
   "startDate":"2021-05-30",
   "endDate":"2021-06-01",
   "active":true,
   "_links":{
      "self":{
         "href":"http://localhost:8989/v1/reservation/94bf6026-b668-11eb-8529-0242ac130003"
      }
   }
}
{
   "status":"BAD_REQUEST",
   "timestamp":"2021-05-18T02:52:19.10936",
   "message":"No vacant dates available from 2021-05-30 to 2021-06-01"
}
{
   "status":"BAD_REQUEST",
   "timestamp":"2021-05-18T02:52:19.210229",
   "message":"No vacant dates available from 2021-05-30 to 2021-06-01"
}
```

