#!/usr/bin/env bash

set -e

base_url="$1"

payload_request1='{"uuid": "94bf6026-b668-11eb-8529-0242ac130003","email": "dev1@gmail.com","fullName": "Developer_1","startDate": "2021-05-30","endDate": "2021-06-01"}'
echo "Payload 1: $payload_request1"

payload_request2='{"uuid": "94bf63aa-b668-11eb-8529-0242ac130003","email": "dev2@gmail.com","fullName": "Developer_2","startDate": "2021-05-30","endDate": "2021-06-01"}'
echo "Payload 2: $payload_request2"

payload_request3='{"uuid": "94bf62ba-b668-11eb-8529-0242ac130003","email": "dev3@gmail.com","fullName": "Developer_3","startDate": "2021-05-30","endDate": "2021-06-01"}'
echo "Payload 3: $payload_request3"

curl -H "Content-Type: application/json" -d "$payload_request1" "$base_url"/v1/reservation & \
  curl -H "Content-Type: application/json" -d "$payload_request2" "$base_url"/v1/reservation & \
  curl -H "Content-Type: application/json" -d "$payload_request3" "$base_url"/v1/reservation &