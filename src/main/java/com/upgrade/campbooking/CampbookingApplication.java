package com.upgrade.campbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampbookingApplication.class, args);
	}

}
