package com.upgrade.campbooking.errorhandlers;

public class BookingIllegalStateException extends RuntimeException {

    private static final long serialVersionUID = 2615054793144819372L;

    public BookingIllegalStateException(String message) {
        super(message);
    }
}
