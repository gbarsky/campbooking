package com.upgrade.campbooking.errorhandlers;


public class BookingNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -479675961293170489L;

    public BookingNotFoundException(String message) {
        super(message);
    }
}

