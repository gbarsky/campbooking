package com.upgrade.campbooking.errorhandlers;


import java.io.Serializable;

public class DatesNotAvailableException extends RuntimeException {

    private static final long serialVersionUID = 5047582070998247802L;

    public DatesNotAvailableException(String message) {
        super(message);
    }
}

