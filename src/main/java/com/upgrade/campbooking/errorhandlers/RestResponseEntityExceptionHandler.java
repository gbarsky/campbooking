package com.upgrade.campbooking.errorhandlers;

import com.upgrade.campbooking.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StaleObjectStateException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BookingNotFoundException.class)
    protected ResponseEntity<Object> handleBookingNotFound(BookingNotFoundException ex) {

        ErrorResponse ErrorResponse = new ErrorResponse(LocalDateTime.now(),HttpStatus.NOT_FOUND,ex.getMessage(), null);
        return buildResponseEntity(ErrorResponse);
    }

    @ExceptionHandler({BookingIllegalStateException.class,
            DatesNotAvailableException.class,
            IllegalArgumentException.class})
    protected ResponseEntity<Object> handleBookingDatesNotAvailable(RuntimeException ex) {

        ErrorResponse ErrorResponse = new ErrorResponse(LocalDateTime.now(),HttpStatus.BAD_REQUEST,ex.getMessage(), null);
        return buildResponseEntity(ErrorResponse);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        List<String> subErrors = ex.getBindingResult().getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        subErrors.addAll(ex.getBindingResult().getGlobalErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList()));

        ErrorResponse ErrorResponse = new ErrorResponse(LocalDateTime.now(),HttpStatus.BAD_REQUEST,"Http Request Arguments Validation error", subErrors);
        log.error("Validation error for {}", ex.getBindingResult().getTarget());
        return buildResponseEntity(ErrorResponse);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        log.info("{} to {}", servletWebRequest.getHttpMethod(),
                servletWebRequest.getRequest().getServletPath());

        ErrorResponse ErrorResponse = new ErrorResponse(LocalDateTime.now(),HttpStatus.BAD_REQUEST,"Malformed JSON in HTTP request", null);
        return buildResponseEntity(ErrorResponse);
    }

    @ExceptionHandler(StaleObjectStateException.class)
    protected ResponseEntity<Object> handleStaleObjectStateException(StaleObjectStateException ex) {

        ErrorResponse ErrorResponse = new ErrorResponse(LocalDateTime.now(),HttpStatus.CONFLICT,"Optimistic locking error - booking was updated by another transaction", null);
        return buildResponseEntity(ErrorResponse);
    }

    private ResponseEntity<Object> buildResponseEntity(ErrorResponse ErrorResponse) {
        return new ResponseEntity<>(ErrorResponse, ErrorResponse.getStatus());
    }

}

