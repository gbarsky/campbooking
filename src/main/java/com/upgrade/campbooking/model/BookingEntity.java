package com.upgrade.campbooking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.upgrade.campbooking.utils.EndDateValidator;
import com.upgrade.campbooking.utils.MaxPeriodValidator;
import com.upgrade.campbooking.utils.StartDateValidator;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Version;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
@EndDateValidator
@StartDateValidator
@MaxPeriodValidator
@JsonIgnoreProperties(ignoreUnknown = true)
@Generated
public class BookingEntity {
    /**
     * Persistence ID
     */
    private Long id;

    @Version
    @Column(name = "version", nullable = false)
    private Long version;

    /**
     * BOOKING UNIQUE ID
     */
    @EqualsAndHashCode.Include
    @Column(name = "uuid", nullable = false)//, unique = true)
    private UUID uuid;

    @NotEmpty(message = "email is mandatory")
    @Size(max = 50)
    private String email;

    @NotEmpty(message = "Full Name is mandatory")
    @Size(max = 50)
    private String fullName;

    @NotNull(message = "start Date is mandatory")
    @Future(message = "Reservation start date must be a future date")
    private LocalDate startDate;

    @NotNull(message = "end Date is mandatory")
    @Future(message = "Reservation end date must be a future date")
    private LocalDate endDate;

    private boolean active;
}
