package com.upgrade.campbooking.service;

import com.upgrade.campbooking.model.DAO.Booking;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface CampSiteBookingService {

    List<LocalDate> findVacantDays(LocalDate startDate, LocalDate endDate);

    Booking findBookingByUuid(UUID uuid);

    Booking createBooking(Booking booking);

    Booking updateBooking(Booking booking);

    boolean cancelBooking(UUID uuid);

}
