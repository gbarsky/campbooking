package com.upgrade.campbooking.service;

import static com.google.common.base.Preconditions.checkArgument;

import com.upgrade.campbooking.model.DAO.Booking;
import com.upgrade.campbooking.repository.CampSiteBookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import com.upgrade.campbooking.errorhandlers.BookingNotFoundException;
import com.upgrade.campbooking.errorhandlers.DatesNotAvailableException;
import com.upgrade.campbooking.errorhandlers.BookingIllegalStateException;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CampSiteBookingServiceImpl implements CampSiteBookingService{

    @Autowired
    private EntityManager entityManager;

    private CampSiteBookingRepository bookingRepository;

    @Autowired
    public CampSiteBookingServiceImpl(CampSiteBookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocalDate> findVacantDays(LocalDate startDate, LocalDate endDate) {

        LocalDate now = LocalDate.now();
        checkArgument(startDate.isAfter(now), "Start date must be in the future");
        checkArgument(endDate.isAfter(now), "End date must be in the future");
        checkArgument(startDate.isEqual(endDate) || startDate.isBefore(endDate),
                "End date must be equal to start date or greater than start date");

        List<LocalDate> vacantDays = startDate
                .datesUntil(endDate.plusDays(1))
                .collect(Collectors.toList());
        List<Booking> bookings = bookingRepository.findForDateRange(startDate, endDate);

        bookings.forEach(b -> vacantDays.removeAll(b.getBookingDates()));
        return vacantDays;
    }

    @Override
    @Transactional(readOnly = true)
    public Booking findBookingByUuid(UUID uuid) {

        Optional<Booking> booking = bookingRepository.findByUuid(uuid);
        if (booking.isEmpty()) {
            throw new BookingNotFoundException(String.format("Booking was not found for uuid=%s", uuid));
        }
        return booking.get();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Retryable(include = CannotAcquireLockException.class,
            maxAttempts = 2, backoff=@Backoff(delay = 150, maxDelay = 300))
    public Booking createBooking(Booking booking) {

        if (!booking.isNew()) {
            throw new BookingIllegalStateException("New booking must not have persistence id");
        }
        List<LocalDate> vacantDays = findVacantDays(booking.getStartDate(), booking.getEndDate());

        if (!vacantDays.containsAll(booking.getBookingDates())) {
            String message = String.format("No vacant dates available from %s to %s",
                    booking.getStartDate(), booking.getEndDate());
            throw new DatesNotAvailableException(message);
        }
        booking.setActive(true);
        booking = bookingRepository.save(booking);
        //entityManager.flush();
        return booking;
    }

    @Override
    @Transactional
    public Booking updateBooking(Booking booking) {

        Booking persistedBooking = findBookingByUuid(booking.getUuid());

        if (!persistedBooking.isActive()) {
            String message = String.format("Booking with uuid=%s is cancelled", booking.getUuid());
            throw new BookingIllegalStateException(message);
        }
        List<LocalDate> vacantDays = findVacantDays(booking.getStartDate(), booking.getEndDate());
        vacantDays.addAll(persistedBooking.getBookingDates());

        if (!vacantDays.containsAll(booking.getBookingDates())) {
            String message = String.format("No vacant dates available from %s to %s",
                    booking.getStartDate(), booking.getEndDate());
            throw new DatesNotAvailableException(message);
        }
        // cancelBooking method should be used to cancel booking
        booking.setId(persistedBooking.getId());
        booking.setVersion(persistedBooking.getVersion());
        booking.setActive(persistedBooking.isActive());
        return bookingRepository.save(booking);
    }

    @Override
    @Transactional
    public boolean cancelBooking(UUID uuid) {

        Booking booking = findBookingByUuid(uuid);
        booking.setActive(false);
        booking = bookingRepository.save(booking);
        return !booking.isActive();
    }

}
