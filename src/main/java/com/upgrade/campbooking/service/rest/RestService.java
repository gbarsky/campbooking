package com.upgrade.campbooking.service.rest;

import com.upgrade.campbooking.model.BookingEntity;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Tag(name = "Reservation", description = "Camp Site Reservation API")
@RequestMapping("/v1/reservation")
public interface RestService {
    @Operation(summary = "Get available dates within a given period", responses = {
            @ApiResponse(responseCode = "200", description = "Success")})
    @GetMapping(value = "/vacant-dates", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<LocalDate>> getVacantDates(
            @RequestParam(name = "start_date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "end_date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate);

    @Operation(summary = "Fetch reservation for a given ID", responses = {
            @ApiResponse(responseCode = "200", description = "Success"),
            @ApiResponse(responseCode = "404", description = "Not found: reservation for a given reservation ID does not exist")})
    @GetMapping(value = "/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<EntityModel<BookingEntity>> getBooking(@PathVariable() UUID uuid);

    @Operation(summary = "Add new reservation", responses = {
            @ApiResponse(responseCode = "201", description = "Created: new reservation was added"),
            @ApiResponse(responseCode = "400", description = "Bad request: new reservation was not added")})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<EntityModel<BookingEntity>> addBooking(@RequestBody() @Valid BookingEntity bookingEntity);
    @Operation(summary = "Update existing reservation", responses = {
            @ApiResponse(responseCode = "200", description = "Success: reservation was updated"),
            @ApiResponse(responseCode = "400", description = "Bad request: existing reservation was not updated"),
            @ApiResponse(responseCode = "404", description = "Not found: reservation for a given ID does not exist"),
            @ApiResponse(responseCode = "409", description = "Conflict: reservation was updated by another transaction")})
    @PutMapping(value = "/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<EntityModel<BookingEntity>> updateBooking(@PathVariable("uuid") UUID uuid, @RequestBody @Valid BookingEntity bookingEntity);

    @Operation(summary = "Cancel existing reservation", responses = {
            @ApiResponse(responseCode = "200", description = "Success: reservation was cancelled"),
            @ApiResponse(responseCode = "400", description = "Bad request: existing reservation was not updated"),
            @ApiResponse(responseCode = "404", description = "Not found: reservation for a given ID does not exist")})
    @DeleteMapping(value = "/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> cancelBooking (@PathVariable("uuid") UUID uuid);
}
