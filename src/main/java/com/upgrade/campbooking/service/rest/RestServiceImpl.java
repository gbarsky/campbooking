package com.upgrade.campbooking.service.rest;

import com.upgrade.campbooking.model.BookingEntity;
import com.upgrade.campbooking.model.DAO.Booking;
import com.upgrade.campbooking.service.CampSiteBookingService;
import com.upgrade.campbooking.utils.CampSiteBookingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
public class RestServiceImpl implements RestService{

    private CampSiteBookingService campSiteBookingService;

    @Autowired
    public RestServiceImpl(CampSiteBookingService campSiteBookingService) {
        this.campSiteBookingService = campSiteBookingService;
    }

    public ResponseEntity<List<LocalDate>> getVacantDates(LocalDate startDate, LocalDate endDate) {
        if (startDate == null) {
            startDate = LocalDate.now().plusDays(1);
        }
        if (endDate == null) {
            endDate = startDate.plusMonths(1);
        }
        List<LocalDate> vacantDates = campSiteBookingService.findVacantDays(startDate, endDate);
        return new ResponseEntity<>(vacantDates, HttpStatus.OK);
    }

    public ResponseEntity<EntityModel<BookingEntity>> getBooking(UUID uuid) {
        Booking booking = campSiteBookingService.findBookingByUuid(uuid);
        return new ResponseEntity<>(getResource(booking), HttpStatus.OK);
    }

    public ResponseEntity<EntityModel<BookingEntity>> addBooking(BookingEntity bookingEntity) {
        com.upgrade.campbooking.model.DAO.Booking addedBooking = campSiteBookingService.createBooking(CampSiteBookingMapper.INSTANCE.toBooking(bookingEntity));
        EntityModel<BookingEntity> resource = getResource(addedBooking);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(resource.getRequiredLink(IanaLinkRelations.SELF).getHref()));

        return new ResponseEntity<>(resource, headers, HttpStatus.CREATED);
    }

    public ResponseEntity<EntityModel<BookingEntity>> updateBooking(UUID uuid, BookingEntity bookingEntity) {
        com.upgrade.campbooking.model.DAO.Booking updatedBooking = campSiteBookingService.updateBooking(
                CampSiteBookingMapper.INSTANCE.toBooking(bookingEntity));
        return new ResponseEntity<>(getResource(updatedBooking), HttpStatus.OK);
    }

    public ResponseEntity<Void> cancelBooking(UUID uuid) {
        boolean cancelled = campSiteBookingService.cancelBooking(uuid);
        if (cancelled) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private EntityModel<BookingEntity> getResource(Booking booking) {
        EntityModel<BookingEntity> resource = EntityModel.of(CampSiteBookingMapper.INSTANCE.toBookingEntity(booking));
        Link selfLink = WebMvcLinkBuilder
                .linkTo(this.getClass()).slash(booking.getUuid()).withSelfRel();
        resource.add(selfLink);
        return resource;
    }
}
