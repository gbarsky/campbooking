package com.upgrade.campbooking.utils;

import com.upgrade.campbooking.model.BookingEntity;
import com.upgrade.campbooking.model.DAO.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CampSiteBookingMapper {

    CampSiteBookingMapper INSTANCE = Mappers.getMapper(CampSiteBookingMapper.class);

    BookingEntity toBookingEntity(Booking booking);

    Booking toBooking(BookingEntity bookingEntity);

}