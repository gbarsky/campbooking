package com.upgrade.campbooking.utils;

import com.upgrade.campbooking.model.BookingEntity;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EndDateValidatorImpl implements
        ConstraintValidator<EndDateValidator, BookingEntity> {

    @Override
    public void initialize(EndDateValidator constraintAnnotation) {
        // no additional initialization needed
    }

    @Override
    public boolean isValid(BookingEntity bookingEntity, ConstraintValidatorContext constraintValidatorContext) {
        return bookingEntity.getStartDate().isBefore(bookingEntity.getEndDate());
    }
}