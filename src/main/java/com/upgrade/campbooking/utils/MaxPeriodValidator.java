package com.upgrade.campbooking.utils;

import com.upgrade.campbooking.utils.MaxPeriodValidatorImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MaxPeriodValidatorImpl.class)
@Documented
public @interface MaxPeriodValidator {

    String message() default "Reservation stay period is max up to 3 days";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
