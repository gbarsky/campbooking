package com.upgrade.campbooking.utils;

import com.upgrade.campbooking.model.BookingEntity;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.Period;


public class MaxPeriodValidatorImpl
        implements ConstraintValidator<MaxPeriodValidator, BookingEntity> {

    @Override
    public void initialize(MaxPeriodValidator constraintAnnotation) {
        // no additional initialization needed
    }

    @Override
    public boolean isValid(BookingEntity bookingEntity, ConstraintValidatorContext constraintValidatorContext) {
        return Period.between(bookingEntity.getStartDate(), bookingEntity.getEndDate()).getDays() <= 3;
    }
}