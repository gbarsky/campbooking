package com.upgrade.campbooking.utils;

import com.upgrade.campbooking.model.BookingEntity;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;


public class StartDateValidatorImpl
        implements ConstraintValidator<StartDateValidator, BookingEntity> {

    @Override
    public void initialize(StartDateValidator constraintAnnotation) {
        // no additional initialization needed
    }

    @Override
    public boolean isValid(BookingEntity bookingEntity, ConstraintValidatorContext constraintValidatorContext) {
        return LocalDate.now().isBefore(bookingEntity.getStartDate())
                && bookingEntity.getStartDate().isBefore(LocalDate.now().plusMonths(1).plusDays(1));
    }
}
