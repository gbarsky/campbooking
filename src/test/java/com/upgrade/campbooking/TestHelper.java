package com.upgrade.campbooking;

import com.upgrade.campbooking.model.BookingEntity;
import com.upgrade.campbooking.model.DAO.Booking;
import com.upgrade.campbooking.utils.CampSiteBookingMapper;

import java.time.LocalDate;
import java.util.UUID;

public class TestHelper {

  public static final String FULL_NAME = "Developer";
  public static final String EMAIL = "dev1@gmail.com";

  public static BookingEntity buildBookingEntity(LocalDate startDate, LocalDate endDate) {
    return buildBookingDto(startDate, endDate, UUID.randomUUID(), FULL_NAME, EMAIL);
  }
  public static Booking buildBooking(LocalDate startDate, LocalDate endDate) {
    return CampSiteBookingMapper.INSTANCE.toBooking(buildBookingEntity(startDate, endDate));
  }

  public static BookingEntity buildBookingEntity(LocalDate startDate, LocalDate endDate, UUID uuid) {
    return buildBookingDto(startDate, endDate, uuid, FULL_NAME, EMAIL);
  }

  public static Booking buildBooking(LocalDate startDate, LocalDate endDate, UUID uuid) {
    return CampSiteBookingMapper.INSTANCE.toBooking(
        buildBookingDto(startDate, endDate, uuid, FULL_NAME, EMAIL));
  }

  public static BookingEntity buildBookingDto(
      LocalDate startDate, LocalDate endDate, UUID uuid, String fullName, String email) {
    return BookingEntity.builder()
        .startDate(startDate)
        .endDate(endDate)
        .uuid(uuid)
        .fullName(fullName)
        .email(email)
        .active(true)
        .build();
  }
}
